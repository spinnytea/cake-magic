'use strict';
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var cleanCSS = require('gulp-clean-css');
var gulp = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var lazypipe = require('lazypipe');
var less = require('gulp-less');
var lesshint = require('gulp-lesshint');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var templateCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');


var entryPoint = 'client/app.js';
var outputName = 'cake-magic.js';
var js = [ 'client/**/*.js', '!client/lb-services.js' ];
var html = [ 'client/**/*.html', '!client/index.html' ];
var css = [ 'client/**/*.less' ];
var resource = [ 'client/index.html' ];


gulp.task('build-js', ['lint-js'], function () {
  // set up the browserify instance on a task basis
  var b = browserify({
    entries: entryPoint,
    debug: true
  });

  return b.bundle()
    .pipe(source(outputName))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    // Add transformation tasks to the pipeline here.
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('static'));
});
gulp.task('lint-js', [], function () {
  return gulp.src(js).pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('build-html', [], function() {
  return gulp.src(html)
    .pipe(templateCache({ standalone: true }))
    .pipe(gulp.dest('static'));
});

gulp.task('build-css', ['lint-css'], function() {
  return gulp.src(css)
    .pipe(sourcemaps.init())
    .pipe(less())
    .on('error', function() { gutil.log(arguments); this.emit('end'); })
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('static'));
});
gulp.task('lint-css', [], function() {
  return gulp.src(css)
    .pipe(lesshint())
    .pipe(lesshint.reporter())
    .pipe(lesshint.failOnError()); // TODO this doesn't seem to actually fail the build
});

gulp.task('build-resource', [], function() {
  return gulp.src(resource)
    .pipe(gulp.dest('static'));
});

gulp.task('build', ['build-js', 'build-html', 'build-css', 'build-resource'], function() {});
gulp.task('buildd', [], function() {
  gulp.watch(js, ['build-js']);
  gulp.watch(html, ['build-html']);
  gulp.watch(css, ['build-css']);
  gulp.watch(resource, ['build-resource']);
  gulp.start('build');
});

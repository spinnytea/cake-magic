'use strict';
module.exports = angular.module('cake-magic.cake.search', []);

module.exports.controller('cakes-search', [
  '$scope',
  function CakesSearchController($scope) {
    $scope.nested = {
      selection: undefined
    };
  }
]); // end cakes search controller

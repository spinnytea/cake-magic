--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: category; Type: TABLE; Schema: public; Owner: chicarksey
--

CREATE TABLE category (
    name text NOT NULL,
    id integer NOT NULL
);


ALTER TABLE category OWNER TO chicarksey;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: chicarksey
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO chicarksey;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chicarksey
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: chicarksey
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: chicarksey
--

COPY category (name, id) FROM stdin;
Vanilla	1
Chocolate	2
Citrus	3
Brown Sugar	4
Fruit + Veggie	5
Nutty	6
Coconut	7
Mocha	8
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chicarksey
--

SELECT pg_catalog.setval('category_id_seq', 8, true);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: chicarksey
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


Cake Magic
==========

It's not about efficiency, it's about learning to use `loopback.io`.
And I suppose I should make a pretty ui given who this is for.

## useful commands
```bash
# startup the database
npm run dbup
# shut down the database
npm run dbdown

# use loopback to generate the services
npx lb-ng server/server.js client/lb-services.js

# backup the data tables
pg_dump cake-magic -t cake > backup/cake.sql
pg_dump cake-magic -t category > backup/category.sql
```

## TODO <small>(General)</small>
* local database without (not postgress)
  * doesn't need to be started or stopped
  * saves as plaintext (sql) so we can check in updates
* time series chart
  * bar graph for month, year (bargraph, group dates)
  * days, weeks as squares (big grid of squares, like "this is your life in weeks")
  * day-of-week frequency (count all by day of week; basically all saturday and sunday)
* spinners for syrup and frosting
  * more data in the database (syrup and frosting for each cake)
  * I need to verify, but I'm 100% certain the 'category' is just the batter base, so no need for another spinner for that
* iterate on ui
  * page: what can we make next (list of cake suggestions)
  * gulp-eslint
  * put the category color in the database
  * update from cake table is annoying
    * possible solution: put the update in the tr right below, only show upon select
* pictures?
* make features to test loopback
  * how to use `server/boot/root.js`
  * update date directly (rather than whole object)
  * locked rows
    * add a validator
    * once lock is true throw an error on changes
  * cache categories so they aren't queried every time
  * figure out the where operators (where date not null, where name ilike 'string')

## Previous Notes
 * don't try to copy recipes ingredients
   * to do it correctly, it's basically the whole "crafter life"
   * you can make some assumptions to make it simpler
   * but you still need to construct the db by hand (which was always the hard part; you scraped the web for it)

## Setup <small>(for future reference)</small>

For now, I'm delivering the whole project.
In the future, I'd like loopback to generate as much as possible after checking out the repo.
It's be nice if we only need to check in configuration and init scripts.

```
lb app
npm install --save-dev loopback-cli
npm install --save-dev bower
bower install --save angular angular-resources angular-ui-router bootstrap
```
```
// the ever elusive initial configuration of postgres
npm install --save loopback-connector-postgresql
brew install postgresql
initdb ~/db/cake-magic
pg_ctl -D ~/db/cake-magic start
createuser -s postgres

// inside server boot
app.dataSources.postgresStorage.autoupdate();
```
http://loopback.io/doc/en/lb3/PostgreSQL-connector.html
```
lb datasource
// postgres_storage
// postgres
// <empty url>
// localhost
// 5432
// <empty user>
// <empty password>
// cake-magic
```
```
lb model
// cake
// postgres_storage
// PersistedModel
// Y
// cakes
// common
// name
// string
// y
// <empty default>

lb model
// category
// postgres_storage
// PersistedModel
// Y
// categories
// common
// name
// string
// y
// <empty default>

lb relation
// cake
// belongs to
// category
// category
// categoryId
```
```
npm install --save-dev loopback-sdk-angular-cli
lb-ng server/server.js client/lb-services.js
// make dummy client application
```
manually configure server/middleware.json
* https://loopback.io/doc/en/lb3/Add-a-static-web-page.html
* http://apidocs.strongloop.com/loopback-datasource-juggler/#datasource-prototype-automigrate
```
node .
```

## Desired setup

```
npm install
lb app
bower install
...?
```

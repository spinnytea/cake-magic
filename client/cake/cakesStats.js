'use strict';
module.exports = angular.module('cake-magic.cake.stats', []);

module.exports.controller('cakes-stats', [
  '$scope', '$rootScope', '$q', 'Cake', 'Category',
  function CakesStatsController($scope, $rootScope, $q, Cake, Category) {

    // XXX this is really bad design, we need a parent controller that should manage this
    $scope.searchByCategory = function(c) {
      // change tabs to load the new controller
      $scope.site.tab = 'table';
      // then broadcast the event while the controller is listening
      setTimeout(function() {
        $rootScope.$broadcast('table-filter', { category: c.category });
      });
    };

    $q.all({
      cakes: Cake.find({filter:{where:{skip:false}}}).$promise,
      categories: Category.find().$promise,
    }).then(function(loaded) {
      var cakes = loaded.cakes;
      var categories = _.keyBy(loaded.categories, 'id');

      var total = _.chain(cakes)
        .countBy('categoryId')
        .map(function(value, key) { return { category: categories[key].name, count: value }; })
        .value();
      var made = _.chain(cakes)
        .filter('date')
        .countBy('categoryId')
        .map(function(value, key) { return { category: categories[key].name, count: value }; })
        .value();
      var percents = _.values(total).map(function(obj) {
        var m = _.find(made, { category: obj.category });
        return {
          category: obj.category,
          percent: round((m?m.count:0) / obj.count * 100, 2)
        };
      });
      var sumTotal = _.sumBy(total, 'count');
      var sumMade = _.sumBy(made, 'count');
      $scope.overallPercent = round(_.size(_.filter(cakes, 'date')) / _.size(cakes) * 100, 2);

      $scope.outerPie = made;
      $scope.innerPie = total;
      $scope.percents = percents;

      // suggest categories who's "percentage-total" is larger than the "percentage-completed"
      $scope.suggested = _.map(categories, 'name').reduce(function(ret, name) {
        var t = _.find(total, { category: name });
        var m = _.find(made, { category: name });
        ret[name] = (t.count / sumTotal) > (((m && m.count) || 0) / sumMade);
        return ret;
      }, {});
    });
  }
]); // end cakes stats controller

module.exports.directive('donutChart', [function DonutChartDirective() {
  return function DonutChartLink($scope, elem, attr) {
    var width = attr.width,
      height = attr.height,
      radius = Math.min(width, height) / 2,
      ringSize = radius / 3;
    var translate = 'translate(' + width / 2 + ',' + height / 2 + ')';

    // TODO fix colors to categories
    var fallbackColors = d3.scaleOrdinal()
      .range(d3.schemeCategory10);
    var categoryColors = {
      'Vanilla': '#ed5b69',
      'Chocolate': '#89a9c8',
      'Citrus': '#e2d26b',
      'Brown Sugar': '#7e4b65',
      'Fruit + Veggie': '#8fb168',
      'Nutty': '#e8b218',
      'Coconut': '#e892a6',
      'Mocha': '#599eab',
    };
    var color = function(category) {
      if(category in categoryColors) return categoryColors[category];
      return fallbackColors(category);
    };

    var pie = d3.pie()
      .sort(null)
      .value(function(d) { return d.count; });

    // database[ring][category] = { startAngle, endAngle }
    // XXX needs a better name - used to store start/end arcs for rotating the graph
    var database = { 0: {}, 1: {} };

    function buildChart(data, ring) {
      var arc = d3.arc()
        .outerRadius(radius - (ring+0)*ringSize)
        .innerRadius(radius - (ring+1)*ringSize);

      var svg = d3.select(elem[0]).append('g')
        .attr('class', 'ring' + ring)
        .attr('transform', translate);

      var g = svg.selectAll('.arc')
        .data(pie(data))
        .enter().append('g')
        .attr('class', 'arc');

      var moveRings = _.pull(_.keys(database), ''+ring); // select all but my ring
      g.append('path')
        .attr('d', arc)
        .style('fill', function(d) { return color(d.data.category); })
        .each(function(d) { database[ring][d.data.category] = _.pick(d, ['startAngle', 'endAngle']); })
        .on('mousemove', function(d) {
          var mouse = d3.mouse(this);
          // what's the angle to the mouse in the same coord space as d.startAngle and d.endAngle
          var mouse_a = Math.atan2(-mouse[0], mouse[1]) + Math.PI;
          moveRings.forEach(function(r) { rotateRing(d, r, mouse_a); });
        }).on('mouseleave', function() {
          // reset the rotation
          moveRings.forEach(function(r) { d3.select('.ring' + r).attr('transform', translate); });
        });

      // TODO better text alignment (center, rotate?)
      g.append('text')
        .attr('transform', function(d) { return 'translate(' + arc.centroid(d) + ')'; })
        .attr('dy', '.35em')
        .attr('text-anchor', 'middle')
        .style('pointer-events', 'none')
        .text(function(d) { return d.data.category; });
      // tooltip for dev
      g.append('svg:title')
        .text(function(d) { return d.data.count; });

      function rotateRing(d, r, mouse_a) {
        var c = database[r][d.data.category];
        var g = d3.select('.ring' + r);
        var transform = translate;
        if(c) {
          // only rotate as much as needed
          // interpolate the moved ring based on the interpolation of the static ring

          // find the percentage around the static arc
          var p = (mouse_a-d.startAngle) / (d.endAngle-d.startAngle);

          // find the new angle based on the percentage
          // line up that angle with mouse_a
          mouse_a -= (c.endAngle-c.startAngle) * p + c.startAngle;

          // change coord space
          mouse_a = mouse_a / Math.PI * 180;
          transform += ' rotate(' + mouse_a + ')';
        }
        g.attr('transform', transform);
      }
    }

    // TODO $scope.$await
    if('outer' in attr) {
      var unwatchOuter = $scope.$watch(function() { return $scope.$parent.$eval(attr.outer); }, function(data) { if(data) {
        buildChart(data, 0);
        unwatchOuter();
      } });
      $scope.$on('$destroy', unwatchOuter);
    }

    // TODO $scope.$await
    if('inner' in attr) {
      var unwatchInner = $scope.$watch(function() { return $scope.$parent.$eval(attr.inner); }, function(data) { if(data) {
        buildChart(data, 1);
        unwatchInner();
      } });
      $scope.$on('$destroy', unwatchInner);
    }
  };
}]);

module.exports.directive('progress', [function() {
  return {
    restrict: 'C',
    replace: false,
    scope: { percent: '=' },
    template: '<div class="progress-bar" role="progressbar" aria-valuenow="{{percent}}" ' +
      'aria-valuemin="0" aria-valuemax="100" style="width:{{percent}}%">' +
      '<span>{{percent}}% Complete</span>' +
      '</div>'
  };
}]);

function round(number, precision) {
  var factor = Math.pow(10, precision);
  var tempNumber = number * factor;
  var roundedTempNumber = Math.round(tempNumber);
  return roundedTempNumber / factor;
}

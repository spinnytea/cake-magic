'use strict';
module.exports = exports = angular.module('spinnytea.utils', [
  'drahak.hotkeys',
]);

exports.factory('$exceptionHandler', function() {
  return function UncaughtExceptionHandler(exception, cause) {
    if(cause) { exception.message += ' (caused by "' + cause + '")'; }
    throw exception;
  };
});

exports.factory('bindKeys', ['$hotkey', function($hotkey) {
  return function BindKeys($scope, keys) {
    _.forEach(keys, function(fn, key) { $hotkey.bind(key, fn); });
    $scope.$on('$destroy', function() {
      _.forEach(keys, function(fn, key) { $hotkey.unbind(key, fn); });
    });
  };
}]);

exports.directive('btn', [function() {
  return {
    restrict: 'C',
    link: function BtnDisabledTitleLink($scope, elem, attr) {
      if(('title' in attr) && ('ngDisabled' in attr)) {
        var span = elem.wrap('<span/>').parent();
        $scope.$on('$destroy', $scope.$watch(function() { return attr.title; }, function(title) {
          span.attr('title', title);
        }));
      }
    }
  };
}]);

exports.filter('padNumber', function() {
  return function(input, length) {
    input = +input;
    length = +length;
    if(!_.isNumber(input) || !_.isNumber(length))
      return input;

    input = ''+input;
    while(input.length < length)
      input = '0'+input;

    return input;
  };
});

exports.filter('filterAll', ['$filter', function($filter) {
  $filter = $filter('filter');
  return function FilterAll(array, query) {
    if(!_.isString(query)) return array;
    var qs = query.split(' ');

    // only return objects that have ALL the query substrings
    return array.filter(function(a) {
      return qs.every(function(q) {
        return $filter([a], q).length;
      });
    });

  };
}]);

exports.directive('areYouSure', ['$q', function($q) {
  return {
    restrict: 'A',
    replace: true,
    transclude: true,
    scope: { callback: '@areYouSure' },
    template: '<button ng-click="theCheck()" ng-mouseleave="reset()">' +
    '<ng-transclude ng-if="step === 1"></ng-transclude>' +
    '<span ng-if="step === 2"><i class="fa fa-fw fa-question-circle"></i>Are you sure</span>' +
    '<span ng-if="step === 3"><i class="fa fa-fw fa-refresh fa-spin"></i>Working</span>' +
    '<span ng-if="step === 4"><i class="fa fa-fw fa-check"></i>Done</span>' +
    '<span ng-if="step === 5"><i class="fa fa-fw fa-exclamation-circle"></i>Error</span>' +
    '</button>',
    controller: ['$scope', AreYouSureController]
  };

  function AreYouSureController($scope) {
    $scope.step = 1;
    $scope.reset = function() { if($scope.step !== 3) $scope.step = 1; };
    $scope.theCheck = function() {
      if($scope.step === 1) {
        $scope.step = 2;
      } else if($scope.step === 2) {
        $scope.step = 3;
        // promisify the callback
        $q.resolve().then(function() {
          try {
            return $scope.$parent.$eval($scope.callback);
          } catch(e) {
            return $q.reject(e);
          }
        }).then(function() {
          $scope.step = 4;
        }).catch(function() {
          $scope.step = 5;
        });
      }
    };
  }
}]);

exports.directive('booleanToggle', [function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    replace: true,
    scope: {
      trueText: '@',
      falseText: '@',

      ngModel: '=',
    },
    template: '<div class="btn-group">' +
    '<button class="btn btn-default" ng-bind="trueText" ng-class="{\'active\': ngModel}" ng-click="ngModel = true"></button>' +
    '<button class="btn btn-default" ng-bind="falseText" ng-class="{\'active\': !ngModel}" ng-click="ngModel = false"></button>' +
    '</div>',
  };
}]);

exports.directive('thOrderBy', [function() {
  return {
    restrict: 'A',
    scope: {
      thOrderBy: '@',

      ngModel: '=',
    },
    replace: true,
    transclude: true,
    template: '<th ng-click="toggleSort()" class="show-on-hover-wrapper"><ng-transclude></ng-transclude> <i class="fa" ng-class="sortClass()"></i></th>',
    link: function($scope) {
      var asc = $scope.thOrderBy;
      var desc = '-' + asc;
      function isAsc() { return $scope.ngModel === asc; }
      function isDesc() { return $scope.ngModel === desc; }

      $scope.sortClass = function() {
        if(isAsc()) {
          return 'fa-sort-asc';
        } else if(isDesc()) {
          return 'fa-sort-desc';
        } else {
          return 'fa-unsorted text-muted show-on-hover';
        }
      };

      $scope.toggleSort = function() {
        if(isDesc()) {
          $scope.ngModel = '';
        } else if(isAsc()) {
          $scope.ngModel = desc;
        } else {
          $scope.ngModel = asc;
        }
      };
    }
  };
}]);

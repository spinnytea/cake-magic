'use strict';
module.exports = angular.module('cake-magic.category', []);

module.exports.controller('categories', [
  '$scope', 'Category', 'Cake',
  function CategoryController($scope, Category, Cake) {
    var input;
    resetForm();
    getList();

    function getList() {
      Category.find().$promise.then(function(list) {
        $scope.categories = list;
        list.forEach(function(c) {
          Cake.count({ where: { categoryId: c.id, skip: false } }).$promise.then(function(data) {
            c.count = data.count;
          });
        });
      });
    }
    function resetForm() {
      $scope.input = input = {
        name: ''
      };
    }

    $scope.create  = function createCategory() {
      if($scope.input && input.name) {
        $scope.input = null;
        Category.create(input).$promise.then(function() {
          getList();
          resetForm();
        });
      }
    };
  }
]); // end categories controller

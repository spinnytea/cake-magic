--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cake; Type: TABLE; Schema: public; Owner: chicarksey
--

CREATE TABLE cake (
    name text NOT NULL,
    id integer NOT NULL,
    categoryid integer NOT NULL,
    date timestamp with time zone,
    skip boolean NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    notes text,
    rating integer DEFAULT 0 NOT NULL
);


ALTER TABLE cake OWNER TO chicarksey;

--
-- Name: cake_id_seq; Type: SEQUENCE; Schema: public; Owner: chicarksey
--

CREATE SEQUENCE cake_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cake_id_seq OWNER TO chicarksey;

--
-- Name: cake_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: chicarksey
--

ALTER SEQUENCE cake_id_seq OWNED BY cake.id;


--
-- Name: cake id; Type: DEFAULT; Schema: public; Owner: chicarksey
--

ALTER TABLE ONLY cake ALTER COLUMN id SET DEFAULT nextval('cake_id_seq'::regclass);


--
-- Data for Name: cake; Type: TABLE DATA; Schema: public; Owner: chicarksey
--

COPY cake (name, id, categoryid, date, skip, locked, notes, rating) FROM stdin;
Spiced Almond Cake with Cream Cheese Frosting	80	6	\N	f	f	\N	0
Coconut-Caramel Cake with Malted Chocolate Frosting	82	7	\N	f	f	\N	0
Coconut-White Chocolate Cake	83	7	\N	f	f	\N	0
Coconut + Chocolate Cake	85	7	\N	f	f	\N	0
Coconut Ginger + Rum Cake	86	7	\N	f	f	\N	0
Mango-Coconut Cake with Salted Caramel Frosting	87	7	\N	f	f	\N	0
Classic Birthday Cake	11	1	\N	f	f	\N	0
Spicy Cinnamon Cake with Apples	12	1	\N	f	f	\N	0
Coconut + Raspberry Cake with Cream Cheese Frosting	88	7	\N	f	f	\N	0
Italian Cream Cake	89	7	\N	f	f	\N	0
Peaches + Cream Cake	15	1	\N	f	f	\N	0
Warm Chocolate Gingerbread Cake	17	2	\N	f	f	\N	0
Candy Bar Cake	18	2	\N	f	f	\N	0
Chocolate Cake with Tea + Lemon	19	2	\N	f	f	\N	0
Grasshopper Cake	20	2	\N	f	f	\N	0
Salted Caramel Chocolate Cake	22	2	\N	f	f	\N	0
Dark Chocolate Cake with Basil + Caramel	23	2	\N	f	f	\N	0
Chocolate-Orange Cake with Caramel Frosting	24	2	\N	f	f	\N	0
Chocolate-Hazelnut Cake	25	2	\N	f	f	\N	0
Hotness Cake	26	2	\N	f	f	\N	0
Chocolate + Red Wine Cake	27	2	\N	f	f	\N	0
Spicy Hot Chocolate Cake	29	2	\N	f	f	\N	0
Chocolate + Pear Cake	30	2	\N	f	f	\N	0
Cookies + Cream Cake	3	1	\N	f	f	\N	0
Blackberry Buttermilk Cake	4	1	\N	f	f	\N	0
Vanilla Cherry + Chocolate Cake	6	1	\N	f	f	\N	0
Caramel Pineapple Upside-Down Cake	7	1	\N	f	f	\N	0
Buttered Rum Cake with Chocolate Frosting	9	1	\N	f	f	\N	0
Arnold Palmer Cake	33	3	\N	f	f	\N	0
Orange + Cardamom Cake with Honey Frosting	34	3	\N	f	f	\N	0
Strawberry-Lemon Cake	35	3	\N	f	f	\N	0
High Tea Cake	37	3	\N	f	f	\N	0
Tripple Citrus Cake	39	3	\N	f	f	\N	0
Lemon-Ginger Cake with Chocolate Frosting	40	3	\N	f	f	\N	0
Basil + Lemon Cream Cake	42	3	\N	f	f	\N	0
Southern Diner Cake	43	4	\N	f	f	\N	0
Maple Bacon + Chocolate Cake	46	4	\N	f	f	\N	0
S'mores Cake	47	4	\N	f	f	\N	0
Honey-Walnut Cake with Ginger Syrup	48	4	\N	f	f	\N	0
Rum Raisin Cake	50	4	\N	f	f	\N	0
Butterscotch Cake	52	4	\N	f	f	\N	0
Pecan Bun Cake	53	4	\N	f	f	\N	0
Maple Vanilla Brunch Cake	54	4	\N	f	f	\N	0
Browned Butter Cake	55	4	\N	f	f	\N	0
Toffee Cake	56	4	\N	f	f	\N	0
Banana-Maple Cake with Malted Vanilla Frosting	59	5	\N	f	f	\N	0
Fall Harvest Cake	60	5	\N	f	f	\N	0
Apple Cider Cake	61	5	\N	f	f	\N	0
Pumpkin-Chocolate Cake	62	5	\N	f	f	\N	0
Zuccini-Thyme Cake with Lemon Pudding Frosting	63	5	\N	t	f	\N	0
Fresh Blueberry Cake	65	5	\N	t	f	\N	0
Pear Cake with Chocolate Frosting	66	5	\N	t	f	\N	0
Banana-Caramel Cake with Malted Milk Chocolate Frosting	69	5	\N	f	f	\N	0
Apricot Cashew + Cardamom Cake with Honey Frosting	75	6	\N	f	f	\N	0
Almond Cake with Cinnamon + Honey	77	6	\N	f	f	\N	0
Almond Joy Cake	78	6	\N	f	f	\N	0
Pistacho + Lemon Cake	79	6	\N	t	f	\N	0
Mocha Caramel Cake	92	8	\N	f	f	\N	0
Double-Shot Mocha Cake	93	8	\N	f	f	\N	0
Orange Mocha Cake with Cream Cheese Frosting	94	8	\N	f	f	\N	0
Mulberry Street Cake	95	8	\N	f	f	\N	0
New Orleans Cafe Au Lait Cake	97	8	\N	f	f	\N	0
Tiramisu Cake	98	8	\N	f	f	\N	0
Vanilla-Olive Oil Cake with Rosemary + Lemon	14	1	\N	t	t	\N	0
Horchata + Caramel	5	1	\N	f	f	\N	0
Confetti Cake	1	1	\N	f	f	\N	0
Spiced Coffee + Cream Cake	99	8	\N	f	f	\N	0
Malted Chocolate Stout	100	8	\N	f	f	\N	0
Elvis Cake	74	6	\N	f	f	\N	0
Summer Lemon Cake	31	3	\N	f	f	\N	0
Tea with Cream + Sugar Cake	57	4	2017-03-05 00:00:00-05	f	t	Not enough tea flavor. Next time, use the tea maker to brew concentrated tea, use that instead of water.	2
Drunken Tuxedo Cake	21	2	2020-10-31 00:00:00-04	f	t	\N	3
Carrot + Coconut Cake	58	5	\N	f	f	\N	0
School Lunch Cake	71	6	\N	f	f	\N	0
Coconut Cake with Strawberry Frosting	81	7	\N	f	f	\N	0
Mocha Raspberry Cake	91	8	2017-05-20 00:00:00-04	f	t	gluten free - more raspberries, try raspberry filling (more like meringue than jelly)	3
Carrot-Lime Cake with Cream Cheese Frosting	70	5	\N	f	f	\N	0
Coconut Cloud Cake	90	7	2017-06-25 00:00:00-04	f	t	\N	3
Sweet Cream Cake	2	1	2017-04-02 00:00:00-04	f	t	Favorite so far (maybe our audience just likes simple flavors: vanilla)	3
Irish Coffee Cake	96	8	2017-03-19 00:00:00-04	f	t	mild coffee flavor. though most people didn't want a coffee flavor - syrup, you could try using coffee instead of water	2
Chai-Pear Cake with Honey Frosting	67	5	2017-04-09 00:00:00-04	f	t	m-a-zing	3
Creamy Caramel Cake	8	1	2017-02-26 00:00:00-05	f	t	Don't burn the Caramel. Put more icing in the middle.	3
Hot Toddy's Lemon Cake	41	3	\N	f	f	\N	0
Chocolate Chip Cookie Cake	45	4	2017-08-18 00:00:00-04	f	t	\N	3
Soda Fountain Cake	28	2	\N	f	f	\N	0
Peanut Butter Pretzel Cake with Salted Caramel Frosting	72	6	\N	f	f	\N	0
Dinner Party Cake	76	6	2019-02-02 00:00:00-05	f	f	\N	0
Lemon Pucker Cake	36	3	2017-04-23 00:00:00-04	f	t	lemon meringue in the middle makes it perfect - Garrett think's this is the best on yet	3
Dream Cake	84	7	2017-04-16 00:00:00-04	f	t	upon reflection, amazing	3
Cookie-Butter Cake	44	4	2017-06-12 00:00:00-04	f	t	\N	3
Best Blackout Cake	16	2	2017-07-04 00:00:00-04	f	t	\N	2
Root Beer Float Cake	49	4	\N	f	f	\N	0
Pumpkin-Ginger Cake with Cream Cheese Frosting	64	5	2018-10-28 00:00:00-04	f	t	\N	3
Boozy Berry Cake	10	1	2017-10-01 00:00:00-04	f	t	\N	0
Cinnamon-Sugar Cake with Chocolate Frosting	51	4	2018-12-09 00:00:00-05	f	t	\N	2
White Russian Cake	13	1	2019-02-02 00:00:00-05	f	f	\N	0
Peanut Butter Cup Cake	73	6	2017-05-07 00:00:00-04	f	t	do NOT make your own confectioners sugar	2
Blueberry Lemon-Ricotta Cake	32	3	2017-05-14 00:00:00-04	f	t	skip the ricotta, raspberry instead of blueberry - don't put jelly in the frosting - some people thought this was the best yet (raspberry lemonade)	2
Caramel Apple Cake	68	5	2017-07-16 00:00:00-04	f	t	simple flavors, great reception	3
Spiced Grapefruit + Cream Cake	38	3	2018-03-17 00:00:00-04	f	t	i did one less egg, but it went super well with the gluten free citrus	3
\.


--
-- Name: cake_id_seq; Type: SEQUENCE SET; Schema: public; Owner: chicarksey
--

SELECT pg_catalog.setval('cake_id_seq', 100, true);


--
-- Name: cake cake_pkey; Type: CONSTRAINT; Schema: public; Owner: chicarksey
--

ALTER TABLE ONLY cake
    ADD CONSTRAINT cake_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


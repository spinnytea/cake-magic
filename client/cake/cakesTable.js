'use strict';
module.exports = angular.module('cake-magic.cake.table', []);

module.exports.controller('cakes-table', [
  '$scope', 'Cake', 'Category',
  function CakesTableController($scope, Cake, Category) {
    var filter = $scope.filter = {
      categoryId: '',
      skip: false
    };
    // TODO this is duplicated in cake.js, we should put it in a service
    $scope.ratings = _.keyBy([
      { id: 0, text: 'None', icon: 'fa-remove' },
      { id: 1, text: 'Bad', icon: 'fa-frown-o' },
      { id: 2, text: 'Ok', icon: 'fa-meh-o' },
      { id: 3, text: 'Good', icon: 'fa-smile-o' },
    ], 'id');

    var categoryPromise = Category.find().$promise.then(function(list) {
      $scope.categories = _.keyBy(list, 'id');
    });

    // TODO combine 'nested' with 'filter'
    // - or document why they are different
    // - filter is for sending to the server? nested is for ui only?
    var nested = $scope.nested = {
      order: '',
      selection: undefined,
    };

    // XXX this is a kludgy event
    // - the event itself makes sense
    // - the timing and broadcast setup is awkward
    $scope.$on('table-filter', function(event, params) {
      categoryPromise.then(function() {
        if(params.category) {
          params.categoryId = _.chain($scope.categories)
            .values()
            .find({ name: params.category })
            .get('id')
            .value();
          delete params.category;
        }
        _.assign(filter, params);
        refresh();
      });
    });

    $scope.select = function select(c) {
      if(nested.selection === c) {
        nested.selection = undefined;
      } else {
        nested.selection = c;
      }
    };

    $scope.showYuck = function() { return filter.skip === null; };
    $scope.toggleYuck = function() {
      if(filter.skip === null)
        filter.skip = false;
      else
        filter.skip = null;
      refresh();
    };

    $scope.list = [];
    $scope.refresh = refresh;
    function refresh() {
      // clean filter
      var params = _.chain(filter)
        .omitBy(function(val) { return val === undefined || val === null || val === ''; })
        .value();

      Cake.find({ filter: { where: params } }).$promise.then(function(list) {
        $scope.list = list;
      });
    }

    refresh();
  }
]); // end cake-table controller

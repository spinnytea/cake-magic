'use strict';
module.exports = angular.module('cake-magic.cake', [
  require('./cakesSearch').name,
  require('./cakesStats').name,
  require('./cakesTable').name
]);

module.exports.directive('cakeSearch', [
  'Cake',
  function(Cake) {
    return {
      restrict: 'A',
      require: 'ngModel',
      scope: true,
      template: '<input class="form-control" list="cakeSearchList" ng-model="selected" />' +
        '<datalist id="cakeSearchList">' +
        '<option ng-repeat="cake in cakes" value="{{cake.name}}"></option>' +
        '</datalist>',
      link: function($scope, elem, attr, ngModelController) {
        $scope.selected = '';
        $scope.cakes = Cake.find({filter:{where:{skip: false}}});
        var ready = $scope.cakes.$promise;

        ngModelController.$render = function() {
          if(ngModelController.$modelValue && _.isString(ngModelController.$modelValue.name)) {
            $scope.selected = ngModelController.$modelValue.name;
          } else {
            $scope.selected = '';
          }
        };

        $scope.$on('$destroy', $scope.$watch('selected', function(value) {
          ready.then(function() {
            ngModelController.$setViewValue(_.find($scope.cakes, { name: value }));
          });
        }));
      }
    };
  }
]);

module.exports.directive('cake', [
  function CakeDirective() {
    return {
      scope: { ngModel: '=' },
      templateUrl: 'cake/cake.html',
      controller: [
        '$scope', 'Cake', 'Category',
        CakeController
      ]
    };

    function CakeController($scope, Cake, Category) {
      var input;
      Category.find().$promise.then(function(list) {
        $scope.categories = _.keyBy(list, 'id');
      });
      $scope.ratings = _.keyBy([
        { id: 0, text: 'None', icon: 'fa-remove' },
        { id: 1, text: 'Bad', icon: 'fa-frown-o' },
        { id: 2, text: 'Ok', icon: 'fa-meh-o' },
        { id: 3, text: 'Good', icon: 'fa-smile-o' },
      ], 'id');

      if($scope.ngModel) {
        $scope.$on('$destroy', $scope.$watch('ngModel', function() {
          $scope.input = input = $scope.ngModel;
        }));
      } else {
        resetForm();
      }

      function resetForm() {
        $scope.input = input = {
          name: '',
          categoryId: null,
          date: null,
          rating: 0,
          notes: '',
          skip: false
        };
      }

      function validInput() {
        return !!input && !!input.name && !!input.name.trim() && _.isNumber(input.categoryId);
      }
      $scope.canCreate = function() { return validInput() && !_.isNumber(input.id); };
      $scope.canUpdate = function() { return validInput() && _.isNumber(input.id); };
      $scope.doCreate = function createCake() {
        if(!$scope.canCreate()) return;
        if(!input.notes) input.notes = null;
        Cake.create(input).$promise.then(function() {
          resetForm();
        });
      };
      $scope.doUpdate = function updateCake() {
        if(!$scope.canUpdate()) return;
        if(!input.notes) input.notes = null;
        input.$save();
      };
      $scope.doLock = function doLock() {
        if(!$scope.canUpdate()) return;
        input.locked = true;
        $scope.doUpdate();
      };
    } // end cake controller
  }
]); // end cake directive

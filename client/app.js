'use strict';
var module = angular.module('cake-magic', [
  require('./cake/cake.js').name,
  require('./category/category.js').name,
  require('./utils').name,
  require('./lb-services'),
  'templates'
]);

module.config([ 'LoopBackResourceProvider', function(LoopBackResourceProvider) {
  // Use a custom auth header instead of the default 'Authorization'
  // LoopBackResourceProvider.setAuthHeader('X-Access-Token');

  // Change the URL where to access the LoopBack REST API server
  LoopBackResourceProvider.setUrlBase('http://localhost:3000/api');
}]);

// input[type="date"] must use ISO String outside and Date inside
// the data source uses the ISO String, so we may as well be consistent
module.directive('input', function DateTypeDirective() {
  return {
    restrict: 'E',
    require: 'ngModel',
    link: function DateTypeLink($scope, elem, attrs, ngModelController) {
      if(!elem.is('input[type="date"]')) return;
      ngModelController.$parsers.push(function parse(value) {
        // convert the date to an ISO string
        if(value) return value.toISOString();
        // need to pass null so the database will delete it
        return null;
      });
      ngModelController.$formatters.push(function format(value) {
        // convert the ISO string to a date
        if(value) return new Date(value);
        // if it's not a date, then it's null
        return null;
      });
    }
  };
});

/**
 * make nav-tabs publish the selected tab
 * use [value] on the li as the key for the selection
 *
 * example:
 * <ul class="nav nav-tabs" data-ng-model="tab">
 *   <li value="table"><a href="#">All Cakes</a></li>
 *   <li value="categories"><a href="#">Categories</a></li>
 * </ul>
 */
module.directive('navTabs', [
  '$compile',
  function NavTabsDirective($compile) {
    return {
      restrict: 'C',
      scope: {
        ngModel: '='
      },
      link: function NavTabsLink($scope, elem) {
        elem.find('>li').each(function() {
          var li = $(this);
          var value = li.attr('value') || li.attr('data-value');
          li.attr('ng-click', 'ngModel = "' + value + '"');
          li.attr('ng-class', '{ active: ngModel==="'+value+'" }');

          // select the first tab if none are selected
          if(!$scope.ngModel) $scope.ngModel = value;
        });

        // recompile the contents so the ng-stuff takes effect
        $compile(elem.contents())($scope);
      }
    };
}
]);
